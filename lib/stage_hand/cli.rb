require 'thor'
require 'sequel'
require 'yaml'
require 'erb'
require 'hashie'
require 'faker'
require 'dotenv/load'

module StageHand
  class HammerOfTheGods < Thor
    include Thor::Actions

    def self.source_root
      File.join(File.dirname(__FILE__), '..', '..', 'templates')
    end

    def self.exit_on_failure?
      true
    end

    method_option :environment, type: :string, aliases: '-e'
    desc "capture", "grab a new stager copy of your db"
    def capture(dir=".")
      Dir.chdir(dir)

      run 'rake db:structure:dump'

      capture_file = File.join(Dir.pwd, "db", "structure.sql")
      File.open(capture_file, "a") do |file|
        db.tables.each do |table_name|
          next if excluded_tables.include?(table_name)
          items = db[table_name]
          items.each do |item|
            if fields[table_name]
              fields[table_name].each do |field_name, task|
                if task
                  existing_value = item[field_name.to_sym]
                  item[field_name.to_sym] = eval(task)
                else
                  item[field_name.to_sym] = nil
                end
              end
              item
            end
            file.puts "#{items.insert_sql(item)};"
            file.puts
          end
        end
      end
    end


    #desc "upload", "upload your most recent capture to S3"
    #desc "download", "download your most recent capture from S3"

    method_option :environment, type: :string, aliases: '-e'
    method_option :structure_file, type: :string, aliases: '-s'
    method_option :file, type: :string, aliases: '-f'
    desc "load", "overwrite the current db with the most recently downloaded stager db"
    def load(dir=".")
      Dir.chdir(dir)
      run 'rake db:structure:load'
    end

    protected
    def environment
      @environment ||= options[:environment] || ENV['RAILS_ENV'] || ENV['RACK_ENV'] || "development"
    end

    def db
      @db ||= Sequel.connect(YAML.load(ERB.new(File.read(File.join(Dir.pwd, 'config', 'database.yml'))).result)[environment])
    end

    def config_file
      return @config_file if @config_file
      config_file_path = File.join(Dir.pwd, 'config', 'stage_hand.yml')
      if File.exists?(config_file_path)
        @config_file = Hashie::Mash.load(config_file_path)
      else
        @config_file = {}
      end
    end

    def excluded_tables
      return @excluded_tables if @excluded_tables
      @excluded_tables = config_file[:excluded_tables] || []
      @excluded_tables.collect! { |t| t.to_sym }
      # always skip schema_migrations, they get set by the structure file
      @excluded_tables = @excluded_tables | [:schema_migrations]
      return @excluded_tables
    end

    def fields
      return @fields if @fields
      # prototype is people: [{name: "Faker::Name.unique.name"}, {email: false}]
      # key is table name
      # fake can be any executable faker string
      # if fake is not set, no edit is made
      # skip defaults to false if not set
      # you should set either fake or skip, not both... duh
      @fields = config_file[:fields] || {}
      return @fields
    end

  end
end
