
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "stage_hand/version"

Gem::Specification.new do |spec|
  spec.name          = "stage_hand"
  spec.version       = StageHand::VERSION
  spec.authors       = ["Rob Kaufman"]
  spec.email         = ["rob@notch8.com"]

  spec.summary       = %q{Stage Hand backups and restores databases for staging. Allows scrubbing of records.}
  spec.description   = %q{Perform backups and restores of databases, scrubbing records along the way. This allows taking limited snapshots of large databases and keeps customer info out of your staging site.}
  spec.homepage      = "https://gitlab.com/notch8/stage_hand"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_development_dependency "pry"

  spec.add_runtime_dependency "dotenv", "~> 2.2"
  spec.add_runtime_dependency "thor", "~> 0.19"
  spec.add_runtime_dependency "sequel", "~> 4.41.0"
  spec.add_runtime_dependency "hashie", "~> 3.5"
  spec.add_runtime_dependency "faker", "~> 1.9"
end
