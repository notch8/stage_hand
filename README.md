# StageHand

Perform backups and restores of databases, scrubbing records along the way. This allows taking limited snapshots of large databases and keeps customer info out of your staging site.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'stage_hand'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install stage_hand

## Usage

Generate an example config file

`stage_hand install`

Create a new, modified copy of the production database

`stage_hand capture -e production`

Load the current database file, currently just does `rake db:structure:load` but may be exapnded in the future

`stage_hand load -e staging`

## Configuration

StageHand uses a configuration file found in config/stage_hand.yml.  There are two sections of this file.

### excluded_tables

This is a list of all the tables to exclude. These tables are skipped by data insertion, but they are reset to the current schema currently. schema_migrations is always in the excluded list, though the structure load will set the values of that table to the most recent migration.

```yaml
excluded_tables:
  - sites
  - secrets
```

### fields

Here is where the real magic happens. You can specify fields to either be set to blank (by using nil or false) or by evaluating code.  [Faker](https://github.com/stympy/faker) is included to make doing so easy. There is also a special variable called `existing_value` which contains the value before any changes. These fields will be changed in flight to the sql file and thus sensitive data can be removed from the production data dump.

```yaml
fields:
  users:
    email: "existing_value.match('notch8.com') ? existing_value : Faker::Internet.unique.email"
    zip: false
  action_items:
    name: "Faker::Name.unique.name"
```

## TODO

-[ ] File upload
-[ ] File Download
-[ ] Ability to limit the number of rows to insert per table

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://gitlab.com/notch8/stage_hand. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the StageHand project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/[USERNAME]/stage_hand/blob/master/CODE_OF_CONDUCT.md).
